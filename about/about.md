# About ...

## Who am I?

My name is Matti Jukola. I currently live in Eindhoven, Netherlands and work
as a software engineer for a Navinfo Europe - Advanced Research Lab.
I have been working professionally with machine learning / computer vision / image
based measurement and data science since 2008. I spend most of my spare time coding,
learning, doing electronics, exercising and skydiving.

# Contacting

You can contact me by:

* Email: contact@buq2.com
* [Linkedin](https://fi.linkedin.com/in/jukolam)
* [Github](https://github.com/buq2)
